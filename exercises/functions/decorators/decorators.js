/**
 * Returns the invocation context after executing the function.
 */
export function fluent(fun) {
	return function(...args) {
		throw new Error('Not implemented')
	}
}

/**
 * Keeps a cache of previously returned values,
 * so that the return value is computed only once
 * per set of arguments.
 */
export function memoise(fun) {
	return function(...args) {
		throw new Error('Not implemented')
	}
}

/**
 * Modifies the function so that it can only be called once.
 */
export function once(fun) {
	return function(...args) {
		throw new Error('Not implemented')
	}
}

/**
 * Takes a list of predicates and only executes the function
 * if they all return true.
 */
export function provided(...preds) {
	return function(fun) {
		return function(...args) {
			throw new Error('Not implemented')
		}
	}
}

/**
 * Modifies the function to take exactly one argument.
 */
export function unary(fun) {
	throw new Error('Not implemented')
}

/**
 * Uakes a list of predicates and only executes the function
 * if they all return false.
 */
export function unless(...preds) {
	return function(fun) {
		return function(...args) {
			throw new Error('Not implemented')
		}
	}
}
