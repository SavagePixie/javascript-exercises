/**
 * Returns the invocation context after executing the function.
 */
export function fluent(fun) {
	return function(...args) {
		fun.apply(this, args)
		return this
	}
}

/**
 * Keeps a cache of previously returned values,
 * so that the return value is computed only once
 * per set of arguments.
 */
export function memoise(fun) {
	const cache = {}
	return function(...args) {
		const key = JSON.stringify(args)
		cache[key] ??= fun.apply(this, args)
		return cache[key]
	}
}

/**
 * Modifies the function so that it can only be called once.
 */
export function once(fun) {
	let executed = false
	return function(...args) {
		if (executed) return
		executed = true
		return fun.apply(this, args)
	}
}

/**
 * Takes a list of predicates and only executes the function
 * if they all return true.
 */
export function provided(...preds) {
	return function(fun) {
		return function(...args) {
			return preds.every(pred => pred.apply(this, args))
				? fun.apply(this, args)
				: undefined
		}
	}
}

/**
 * Modifies the function to take exactly one argument.
 */
export function unary(fun) {
	return fun.length === 1
		? fun
		: function(value) {
			return fun.call(this, value)
		}
}

/**
 * Uakes a list of predicates and only executes the function
 * if they all return false.
 */
export function unless(...preds) {
	return function(fun) {
		return function(...args) {
			return preds.some(pred => pred.apply(this, args))
				? undefined
				: fun.apply(this, args)
		}
	}
}
