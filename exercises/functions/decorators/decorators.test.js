import tap from 'tap'

import { fluent, memoise, once, provided, unary, unless } from './decorators.js'

tap.test('Functions/Decorators', t => {
	t.test('fluent', t => {
		t.test('should return execution context', t => {
			function Proto () {}
			Proto.prototype.withFluent = fluent(function(x) {
				return x + 1
			})
			const instance = new Proto()

			t.ok(instance === instance.withFluent(4))
			t.end()
		})

		t.end()
	})

	t.test('memoise', t => {
		t.test('should only execute function one with same params', t => {
			let counter = 0
			const tester = memoise(x => {
				counter++
				return x + counter
			})

			const result1 = tester(1)
			const result2 = tester(1)
			t.ok(result1 === 2)
			t.ok(result2 === 2)
			t.end()
		})

		t.test('shoul execute function again if params are different', t => {
			let counter = 0
			const tester = memoise(x => {
				counter++
				return counter
			})

			const result1 = tester(1)
			const result2 = tester(2)
			t.ok(result1 === 1)
			t.ok(result2 === 2)
			t.end()
		})

		t.end()
	})

	t.test('once', t => {
		t.test('should only execute the function once', t => {
			let counter = 0
			const tester = once(() => {
				if (tester.executed) t.fail()
				tester.executed = true
				counter++
			})

			tester()
			tester()

			t.ok(counter === 1)
			t.end()
		})

		t.end()
	})

	t.test('provided', t => {
		t.test('should execute the function if all predicates pass', t => {
			const tester = provided(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => 'success')
			t.ok(tester(4) === 'success')
			t.end()
		})

		t.test('should not execute the function if one predicate fails', t => {
			const tester = provided(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => t.fail())
			t.ok(tester(8) === undefined)
			t.end()
		})

		t.test('should not execute the function if all predicates fail', t => {
			const tester = provided(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => t.fail())
			t.ok(tester('potato') === undefined)
			t.end()
		})

		t.end()
	})

	t.test('unary', t => {
		t.test('should turn a function into an unary function', t => {
			const tester = unary(parseInt)
			const result = ['1', '2', '3'].map(tester)
			t.strictSame([1, 2, 3], result)
			t.ok(tester.length === 1)
			t.end()
		})

		t.test('should not change a functions that is already unary', t => {
			const tester = unary(x => x + 1)
			t.ok(tester.length === 1)
			t.end()
		})

		t.end()
	})

	t.test('unless', t => {
		t.test('should execute the function if all predicates fail', t => {
			const tester = unless(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => 'success')
			t.ok(tester('potato') === 'success')
			t.end()
		})

		t.test('should not execute the function if one predicate passes', t => {
			const tester = unless(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => t.fail())
			t.ok(tester(13) === undefined)
			t.end()
		})

		t.test('should not execute the function if all predicates pass', t => {
			const tester = unless(
				x => typeof x === 'number',
				x => x % 2 === 0,
				x => x < 5
			)(() => t.fail())
			t.ok(tester(2) === undefined)
			t.end()
		})

		t.end()
	})

	t.end()
})
