/**
 * Adds a new node to the given tree.
 * Returns the same tree that was passed.
 *
 * @param {TreeNode} tree - Tree to update
 * @param {TreeNode} node - Node to add
 * @returns {TreeNode}
 */
const add = (tree, node) => {
	throw new Error('Not implemented')
}

/**
 * Drops the node that matches the given ID from the tree.
 * Returns the dropped node or null if it doesn't exist.
 *
 * @param {TreeNode} tree - Tree to update
 * @param {string | number | symbol} id - Id to drop
 * @returns {TreeNode | null}
 */
const drop = (tree, id) => {
	throw new Error('Not implemented')
}

/**
 * Finds a node in the given tree by its ID.
 * Returns null if the node doesn't exist.
 *
 * @param {TreeNode} tree - Tree to search
 * @param {string | number | symbol} id - Id to look for
 * @returns {TreeNode | null}
 */
const findById = (tree, id) => {
	throw new Error('Not implemented')
}

/**
 * Returns the total number of nodes in the tree.
 *
 * @param {TreeNode} tree - Tree to count
 * @returns {number}
 */
const size = tree => {
	throw new Error('Not implemented')
}

/**
 * Visit each node in the given tree,
 * calling the given function for each node.
 * Can be halted by returning `false` from the function.
 * Returns the same tree that was passed.
 *
 * @param {TreeNode} tree - Tree to traverse
 * @param {Function} fun - Function to call
 * @returns {TreeNode}
 *
 * @example
 * const tree = new TreeNode(1, 'potato', [
 * 	new TreeNode(2, 'onion'),
 * 	new TreeNode(3, 'carrot')
 * ])
 * Tree.traverse(tree, ({ data }) => {
 * 	console.log(data)
 * 	// Will stop after visiting the first node
 * 	return data !== 'potato'
 * })
 */
const traverse = (tree, fun) => {
	throw new Error('Not implemented')
}

export const Tree = {
	add,
	drop,
	findById,
	size,
	traverse
}
