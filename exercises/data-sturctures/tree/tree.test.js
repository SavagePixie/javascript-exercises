import tap from 'tap'

import { Tree } from './tree.js'

function TreeNode (id, data, children = []) {
	this.id = id
	this.children = children
	this.data = data
}


tap.test('DataStrctures/Tree', t => {
	let tree

	t.beforeEach(() => {
		tree = new TreeNode('root', 'root', [
			new TreeNode('1', 'test-1', [
				new TreeNode('11', 'test-11'),
				new TreeNode('12', 'test-12'),
				new TreeNode('13', 'test-13')
			]),
			new TreeNode('2', 'test-2'),
			new TreeNode('3', 'test-3', [
				new TreeNode('31', 'test-31'),
				new TreeNode('32', 'test-32', [
					new TreeNode('321', 'test-321'),
					new TreeNode('322', 'test-322')
				])
			])
		])
	})

	t.test('add', t => {
		t.test('should add node to tree', t => {
			const result = Tree.add(tree, new TreeNode('4', 'test-4'))
			t.ok(result === tree)
			t.ok(Tree.findById(tree, '4') instanceof TreeNode)
			t.end()
		})

		t.end()
	})

	t.test('drop', t => {
		t.test('should return the removed element', t => {
			const result = Tree.drop(tree, '13')
			t.ok(result.id === '13')
			t.ok(result.data === 'test-13')
			t.ok(result.children.length === 0)
			t.end()
		})

		t.test('should return null if the element does not exist', t => {
			t.ok(Tree.drop(tree, 'not-found') === null)
			t.end()
		})

		t.test('should drop the node from the tree', t => {
			Tree.drop(tree, '2')
			t.ok(Tree.findById(tree, '2') === null)
			t.end()
		})

		t.end()
	})

	t.test('findById', t => {
		t.test('should return the first element matching the id', t => {
			const result = Tree.findById(tree, '2')
			t.ok(result.id === '2')
			t.ok(result.data === 'test-2')
			t.end()
		})

		t.test('should return null if the element is not found', t => {
			t.ok(Tree.findById(tree, 'not-found') === null)
			t.end()
		})

		t.test('should be able to find the root element', t => {
			t.ok(Tree.findById(tree, 'root') === tree)
			t.end()
		})

		t.end()
	})

	t.test('size', t => {
		t.test('should return the number of nodes in the tree', t => {
			t.ok(Tree.size(tree) === 11)
			t.end()
		})

		t.test('should return 1 if there is only the root node', t => {
			t.ok(Tree.size(new TreeNode('test', 'test')), 1)
			t.end()
		})

		t.end()
	})

	t.test('traverse', t => {
		t.test('should pass through every node in the tree in order', t => {
			const expected = ['root', '1', '11', '12', '13', '2', '3', '31', '32', '321', '322']
			const result = []
			Tree.traverse(tree, ({ id }) => result.push(id))
			t.strictSame(result, expected)
			t.end()
		})

		t.test('should stop if callback returns false', t => {
			const callback = () => {
				if (callback.stopped) return void t.fail()
				callback.stopped = true
				return false
			}
			Tree.traverse(tree, callback)
			t.end()
		})

		t.end()
	})

	t.end()
})
