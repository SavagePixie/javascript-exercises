// Decorators

const fluent = fun =>
	(first, ...rest) => {
		fun(first, ...rest)
		return first
	}

// Methods

const add = fluent((tree, node) => tree.children.push(node))

const drop = (tree, id) => {
	if (id === tree.id) return tree

	let state = null
	traverse(tree, node => {
		const index = node.children.findIndex(child => child.id === id)
		if (index === -1) return
		const [ next ] = node.children.splice(index, 1)
		state = next
		return false
	})
	return state
}

const findById = (tree, id) => {
	let state = null
	traverse(tree, node => {
		if (node.id !== id) return
		state = node
		return false
	})
	return state
}

const size = tree => {
	let count = 0
	traverse(tree, () => count++)
	return count
}

const traverse = fluent((tree, fun) => _traverse(tree, fun))

const _traverse = (tree, fun) => {
	let state = fun(tree)
	for (let i = 0; i < tree.children.length; i++) {
		if (state === false) return false
		state = _traverse(tree.children[i], fun)
	}
	return state
}

// Export

export const Tree = {
	add,
	drop,
	findById,
	size,
	traverse
}
